Django==2.0.2
django-filter==1.1.0
djangorestframework==3.7.7
Markdown==2.6.11
Pillow==5.0.0
psycopg2==2.7.3.2
pytz==2017.3
