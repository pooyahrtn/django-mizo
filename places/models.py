from django.db import models
import uuid as uuid_lib
from django.contrib.postgres.fields import ArrayField


class Place(models.Model):
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False)

    latitude = models.FloatField(default=0.0, null=False, blank=False)
    longitude = models.FloatField(default=0.0, null=False, blank=False)
    name = models.CharField(max_length=40)
    image = models.ImageField(null=True, blank=True)
    image_ratio = models.FloatField(default=1.0)
    # have_part = models.BooleanField(default=False, editable=False)

    def __str__(self):
        return self.name


# class Part(models.Model):
#     uuid = models.UUIDField(
#         db_index=True,
#         default=uuid_lib.uuid4,
#         editable=False)
#
#     name = models.CharField(max_length=40)
#     place = models.ForeignKey(Place, on_delete=models.CASCADE)
#     image = models.ImageField(null=True, blank=True)
#     image_ratio = models.FloatField(default=1.0)
#     priority = models.IntegerField(default=1)
#
#     def __str__(self):
#         return self.place.name + ' ' + self.name
#
#     class Meta:
#         ordering = ['-priority',]


class Room(models.Model):
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False)

    name = models.CharField(max_length=40)
    # part = models.ForeignKey(Part, null=True, on_delete=models.SET_NULL, blank=True)
    place = models.ForeignKey(Place, on_delete=models.CASCADE)
    capacity = models.IntegerField(default=0)
    top = models.FloatField(default=0.0, blank=False, null=False)
    left = models.FloatField(default=0.0, blank=False, null=False)
    price_per_unit = models.IntegerField(default=1000)
    description = models.CharField(max_length=400, null=True)

    def __str__(self):
        return self.place.name + ' ' + self.name


class RoomImage(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE, related_name='images')
    image = models.ImageField()