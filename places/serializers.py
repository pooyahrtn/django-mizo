from rest_framework import serializers
from . import models
from reserve.serializers.PackageSerializers import PackageSerializer


class PlaceSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Place
        fields = ('uuid', 'latitude', 'longitude', 'name',  'image', 'image_ratio')


# class PartSerializer(serializers.ModelSerializer):
#
#     class Meta:
#         model = models.Part
#         fields = ('uuid', 'name', 'image', 'image_ratio')


class RoomImage(serializers.ModelSerializer):
    class Meta:
        model = models.RoomImage
        fields = ('image',)


class RoomSerializer(serializers.ModelSerializer):
    images = RoomImage(many=True)
    packages = PackageSerializer(many=True)

    class Meta:
        model = models.Room
        fields = ('uuid', 'name','top', 'left', 'images', 'packages', 'capacity')


class ReviewRoomSerializer(serializers.ModelSerializer):
    place = PlaceSerializer(read_only=True)

    class Meta:
        model = models.Room
        fields = ('place', 'name', 'top', 'left')
