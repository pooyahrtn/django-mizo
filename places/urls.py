from django.conf.urls import url
from .views import *

urlpatterns = [
    # url(r'^parts/(?P<uuid>[0-9A-Fa-f-]+)/$', Parts.as_view()),
    url(r'^room/(?P<uuid>[0-9A-Fa-f-]+)/$', RoomDetail.as_view()),
    # url(r'^rooms/part/(?P<uuid>[0-9A-Fa-f-]+)/$', RoomsFromPart.as_view()),
    url(r'^rooms/place/(?P<uuid>[0-9A-Fa-f-]+)/$', RoomFromPlace.as_view()),
    url(r'^$', Places.as_view()),

]