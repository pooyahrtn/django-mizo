from django.contrib import admin
from . import models


class ImageAdmin(admin.StackedInline):
    model = models.RoomImage


@admin.register(models.Room)
class RoomAdmin(admin.ModelAdmin):
    inlines = [ImageAdmin, ]


class RoomInLine(admin.StackedInline):
    model = models.Room


@admin.register(models.Place)
class RoomAdmin(admin.ModelAdmin):
    inlines = [RoomInLine, ]
