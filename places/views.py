from django.shortcuts import render
from rest_framework import generics, status
from . import models
from . import serializers


class Places(generics.ListAPIView):
    queryset = models.Place.objects.all()
    serializer_class = serializers.PlaceSerializer




class RoomFromPlace(generics.ListAPIView):
    queryset = models.Room.objects.all()
    lookup_field = 'uuid'
    serializer_class = serializers.RoomSerializer

    def filter_queryset(self, queryset):
        return queryset.filter(place__uuid=self.kwargs['uuid'])


class RoomDetail(generics.RetrieveAPIView):
    queryset = models.Room.objects.all()
    lookup_field = 'uuid'
    serializer_class = serializers.RoomSerializer


