# Generated by Django 2.0.2 on 2018-03-19 13:12

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('places', '0009_room_description'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='part',
            name='place',
        ),
        migrations.RemoveField(
            model_name='place',
            name='have_part',
        ),
        migrations.RemoveField(
            model_name='room',
            name='part',
        ),
        migrations.DeleteModel(
            name='Part',
        ),
    ]
