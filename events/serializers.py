from rest_framework import serializers
from . import models


class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Event
        fields = ('uuid', 'title', 'show_on_home', 'price', 'description', 'occur_time', 'image')