from . import models
from . import serializers
from rest_framework import generics


class AllEvents(generics.ListAPIView):
    serializer_class = serializers.EventSerializer
    queryset = models.Event.objects.all()


class HomeEvents(generics.ListAPIView):
    serializer_class = serializers.EventSerializer
    queryset = models.Event.objects.all()

    def filter_queryset(self, queryset):
        return queryset.filter(show_on_home=True)