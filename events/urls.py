from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^all/$', views.AllEvents.as_view()),
    url(r'^home/$', views.HomeEvents.as_view()),
]
