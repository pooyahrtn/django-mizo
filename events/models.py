from django.db import models
import uuid as uuid_lib


class Event(models.Model):
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False)

    title = models.CharField(max_length=60)
    show_on_home = models.BooleanField(default=False)
    price = models.IntegerField(default=0)
    description = models.CharField(max_length=800)
    send_notification = models.BooleanField(default=False)
    occur_time = models.DateTimeField(blank=True, null=True)
    image = models.ImageField(null=True, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['-id']
