from rest_framework import serializers
from . import models


class GetJobWithUUIDSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Job
        fields = ('uuid',)


class JobSerializer(serializers.ModelSerializer):
    uuid = serializers.UUIDField(read_only=True, )
    request_title = serializers.CharField(write_only=True)

    class Meta:
        model = models.Job
        fields = ('uuid', 'confirmed', 'title', 'request_title')
        read_only_fields = ('uuid', 'confirmed', 'title')
        lookup_field = 'uuid'
