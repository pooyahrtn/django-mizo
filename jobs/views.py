
from rest_framework.response import Response

from jobs.models import Job
from . import models
from . import serializers
from rest_framework import generics, status
from profiles.permissions import IsConfirmedUserOrReadOnly


class Jobs(generics.ListCreateAPIView):
    queryset = models.Job.objects.all()
    serializer_class = serializers.JobSerializer
    permission_classes = (IsConfirmedUserOrReadOnly,)

    def filter_queryset(self, queryset):
        return queryset.filter(confirmed=True)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        job = self.perform_create(serializer)
        serializer = self.serializer_class(job)

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        user = self.request.user
        job, created = Job.objects.get_or_create(title=serializer.validated_data['request_title'])
        user.job = job
        user.save()
        return job
