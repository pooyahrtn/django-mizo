from django.db import models
import uuid as uuid_lib


class Job(models.Model):
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False)

    title = models.CharField(max_length=40, unique=True)
    confirmed = models.BooleanField(default=False)

    def __str__(self):
        return self.title
