
def rank_from_profile(profile):
    value = 0
    if profile.linkedIn != '':
        value += 1
    if profile.instagram != '':
        value += 1
    if profile.bio != '':
        value += 2
    if profile.profile_pic.name:
        value += 3
    if profile.job:
        value += 3

    return value