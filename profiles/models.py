from django.db import models
from django.contrib.auth.models import AbstractUser
from jobs.models import Job
import uuid as uuid_lib


class Profile(AbstractUser):
    money = models.BigIntegerField(default=0)
    public_profile = models.BooleanField(default=False)
    public_phone = models.BooleanField(default=False)
    public_reservation = models.BooleanField(default=False)
    linkedIn = models.CharField(max_length=100, blank=True)
    instagram = models.CharField(max_length=100, blank=True)
    unread_notification = models.BooleanField(default=False)
    job = models.ForeignKey(Job, on_delete=models.SET_NULL, blank=True, null=True)
    profile_pic = models.ImageField(null=True, blank=True)
    # todo: add editable = False
    phone_number_confirm_code = models.IntegerField(blank=True, null=True)
    phone_number_confirm_last_try = models.DateTimeField(editable=False, auto_now=True)
    phone_confirmed = models.BooleanField(default=False)
    bio = models.TextField(max_length=400, null=True, blank=True)
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False)
    rank = models.IntegerField(default=0)

    class Meta:
        ordering = ['-rank',]