from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^sign_up/$', SignUp.as_view()),
    url(r'^me/$', MyProfile.as_view()),
    url(r'^active/$', ActiveUser.as_view()),
    url(r'^login/$', Login.as_view()),
    url(r'^request_resend_confirm/$', ResendConfirmCode.as_view()),
    url(r'^update_profile_photo/$', UpdateProfilePhoto.as_view()),
    url(r'^update_profile/$', UpdateProfile.as_view()),
    url(r'^set_user_job/$', SetUserJob.as_view()),
    url(r'users/(?P<uuid>[0-9A-Fa-f-]+)/$', SelectedJobPeopleList.as_view()),
    url(r'users/$', PeopleList.as_view()),

]