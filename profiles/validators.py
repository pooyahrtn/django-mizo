import re
from django.core.exceptions import ValidationError


def validate_phone(value):
    if not re.match(r'^(09)(\d){9}$', value):
        raise ValidationError(
            '%(value)s is not a valid phone number',
            params={'value': value},
        )

