import datetime
from django.shortcuts import get_object_or_404
from rest_framework import generics, parsers
from rest_framework.response import Response
from rest_framework.views import APIView
import random
from jobs.models import Job
from .models import Profile
from .serializers import ProfileSerializer, ActiveUserSerializer, \
    MyProfileSerializer, GetUserWithUsernamPassword, UpdateProfilePhotoSerializer, UpdateProfileSerializer
from . import serializers
from . import permissions
from django.contrib.auth import get_user_model
from .serializers import SignUpSerializer
from rest_framework.authtoken.models import Token
from .utils import rank_from_profile


class PeopleList(generics.ListAPIView):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    lookup_field = 'uuid'

    def filter_queryset(self, queryset):
        return queryset.filter(public_profile=True)


class SelectedJobPeopleList(PeopleList):
    lookup_field = 'uuid'

    def filter_queryset(self, queryset):
        return queryset.filter(public_profile=True, job__uuid=self.kwargs['uuid'])


class SignUp(APIView):
    throttle_classes = ()
    permission_classes = ()
    serializer_class = SignUpSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        token = Token.objects.create(user=user)

        return Response({'token': token.key})


class Login(APIView):
    serializer_class = GetUserWithUsernamPassword

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        code = 210
        if user.phone_confirmed:
            code = 200

        return Response({'token': token.key}, code)


class ActiveUser(APIView):
    queryset = get_user_model().objects.all()
    serializer_class = ActiveUserSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        confirm_code = serializer.validated_data['confirm_code']

        user = serializer.validated_data['user']

        if user.phone_number_confirm_code == confirm_code:
            user.phone_confirmed = True
            user.save()
            return Response({}, status=200)
        return Response({
            "detail": "incorrect"
        }, 414)


class ResendConfirmCode(APIView):
    serializer_class = GetUserWithUsernamPassword

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']

        now = datetime.datetime.now(datetime.timezone.utc)
        delta_time = now - user.phone_number_confirm_last_try

        if delta_time < datetime.timedelta(minutes=2):
            return Response({'sent': False, 'reason': 'you just requested'}, 414)

        code = random.randint(100000, 999999)
        user.phone_number_confirm_code = code
        user.phone_number_confirm_last_try = now
        user.save()
        # todo: send the code

        return Response({'status': 'sent'}, 200)


class MyProfile(generics.RetrieveAPIView):
    queryset = get_user_model().objects.all()
    serializer_class = MyProfileSerializer
    permission_classes = (permissions.IsConfirmedUser,)

    def get_object(self):
        return self.request.user


class UpdateProfilePhoto(generics.UpdateAPIView):
    queryset = get_user_model().objects.all()
    serializer_class = UpdateProfilePhotoSerializer
    permission_classes = (permissions.IsConfirmedUser,)

    def get_object(self):
        return self.request.user


class UpdateProfile(generics.UpdateAPIView):
    queryset = get_user_model().objects.all()
    serializer_class = UpdateProfileSerializer
    permission_classes = (permissions.IsConfirmedUser,)

    def get_object(self):
        return self.request.user

    def perform_update(self, serializer):
        instance = serializer.save()
        instance.rank = rank_from_profile(instance)
        instance.save()


class SetUserJob(APIView):
    serializer_class = serializers.SetUserJobSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        job = get_object_or_404(Job, uuid=serializer.validated_data['job_uuid'])
        user = self.request.user
        user.job = job
        user.save()
        serializer = self.serializer_class(user)
        return Response(serializer.data, 200)


