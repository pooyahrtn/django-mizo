from django.shortcuts import get_object_or_404
from rest_framework import serializers
import random
from . import exceptions

from django.contrib.auth import get_user_model, authenticate
from .validators import validate_phone
from jobs.serializers import GetJobWithUUIDSerializer, JobSerializer
from reserve.serializers.ReserveSerializers import ReserveSerializer

other_profile_without_phones_fields = (
    'linkedIn', 'first_name', 'email', 'instagram', 'profile_pic', 'bio', 'job', 'uuid')

my_profile = ('money', 'username', 'public_profile', 'public_phone', 'reserves') + other_profile_without_phones_fields


class ProfileSerializer(serializers.ModelSerializer):
    job = JobSerializer(read_only=True)
    phone_number = serializers.SerializerMethodField()
    uuid = serializers.UUIDField(read_only=True)
    class Meta:
        model = get_user_model()
        fields = ('phone_number',) + other_profile_without_phones_fields


    def get_phone_number(self, obj):
        if not obj.public_phone:
            return None
        return obj.username


class MyProfileSerializer(serializers.ModelSerializer):
    job = JobSerializer(read_only=True)
    uuid = serializers.UUIDField(read_only=True)
    reserves = ReserveSerializer(many=True)

    class Meta:
        model = get_user_model()
        fields = my_profile


class SignUpSerializer(serializers.Serializer):
    password = serializers.CharField(write_only=True)
    phone_number = serializers.CharField(write_only=True, validators=[validate_phone, ])

    def create(self, validated_data):
        if get_user_model().objects.filter(username=validated_data.get('phone_number')).exists():
            raise exceptions.CreateUserException(code=501, detail='user with this username already exists')

        code = random.randint(100000, 999999)
        user = get_user_model().objects.create_user(username=validated_data.get('phone_number'),
                                                    password=validated_data.get('password'),
                                                    phone_number_confirm_code=code, phone_confirmed=False)

        return user


class ActiveUserSerializer(serializers.Serializer):
    confirm_code = serializers.IntegerField(write_only=True)
    phone_number = serializers.CharField(write_only=True)
    password = serializers.CharField(label="Password", style={'input_type': 'password'})

    def validate(self, attrs):
        username = attrs.get('phone_number')
        password = attrs.get('password')
        if username and password:
            user = authenticate(username=username, password=password)

            if not user:
                msg = 'Unable to log in with provided credentials.'
                raise serializers.ValidationError(msg, code='authorization')
        else:
            msg = 'Must include "username" and "password".'
            raise serializers.ValidationError(msg, code='authorization')

        attrs['user'] = user
        return attrs


class GetUserWithUsernamPassword(serializers.Serializer):
    phone_number = serializers.CharField(label="Username")
    password = serializers.CharField(label="Password", style={'input_type': 'password'})

    def validate(self, attrs):
        username = attrs.get('phone_number')
        password = attrs.get('password')
        if username and password:
            user = authenticate(username=username, password=password)

            if not user:
                msg = 'Unable to log in with provided credentials.'
                raise serializers.ValidationError(msg, code='authorization')
        else:
            msg = 'Must include "username" and "password".'
            raise serializers.ValidationError(msg, code='authorization')

        attrs['user'] = user
        return attrs


class UpdateProfilePhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('profile_pic',)


class UpdateProfileSerializer(serializers.ModelSerializer):
    job = JobSerializer(read_only=True)
    money = serializers.IntegerField(read_only=True)

    class Meta:
        model = get_user_model()
        fields = ('first_name', 'instagram', 'bio', 'linkedIn', 'email', 'job', 'money', 'public_phone', 'public_profile')

    def validate(self, attrs):
        if 'public_profile' in attrs and not 'first_name' in attrs:
            raise serializers.ValidationError('please provide the name')
        if attrs['public_profile'] and attrs['first_name'] == '':
            raise serializers.ValidationError('can not have public profile without name')

        return attrs


class SetUserJobSerializer(serializers.Serializer):
    job_uuid = serializers.UUIDField(write_only=True)
    job = JobSerializer(read_only=True)
