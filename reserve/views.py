from django.shortcuts import render, get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from . import models
from .serializers import ReserveSerializers, peopleInRoomSerializer
from rest_framework import generics
from .utility import free_space_of_package
import datetime
from django.utils import timezone
from django.db import transaction


class ReserveRoom(APIView):
    serializer_class = ReserveSerializers.ReserveSerializer
    permission_classes = (IsAuthenticated,)

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        package = get_object_or_404(models.Package, uuid=serializer.validated_data['package_uuid'])
        # check user money
        if self.request.user.money < package.price:
            return Response({"message": "not enough money", "status": 432}, 432)

        start_time = serializer.validated_data['start_time']
        # todo : check this out: minute = 0 or 30?
        # start_time = start_time.replace(hour=package.start_hour, minute=0, second=0)
        end_time = start_time + datetime.timedelta(hours=package.duration_in_hours)

        if free_space_of_package(package, start_time) < 1:
            return Response({'message': 'the room is full', 'status': '400'}, 430)

        # todo: handle intervals

        if models.Reserve.objects.filter(reserver=self.request.user, room=package.room,
                                         end_time__gte=start_time).exists():
            return Response({'message: you reserved the room in this period'}, 431)

        # todo: move it to manager it's where reserve start to begin
        reserve = models.Reserve.objects.create(package=package, reserver=self.request.user, start_time=start_time,
                                                end_time=end_time, room=package.room)
        models.Occupation.objects.create(reserve=reserve, start_time=start_time, end_time=end_time, room=package.room)
        self.request.user.money -= package.price
        self.request.user.save()
        serializer = ReserveSerializers.ReserveSerializer(reserve)
        return Response(serializer.data, 200)


class PeopleReserveThisRoom(generics.ListAPIView):
    queryset = models.Reserve.objects.all()
    serializer_class = peopleInRoomSerializer.PeopleReserveThisRoomSerializer
    lookup_field = 'uuid'
    pagination_class = None

    def filter_queryset(self, queryset):
        return queryset.filter(room__uuid=self.kwargs['uuid'],
                               end_time__gte=timezone.now())


