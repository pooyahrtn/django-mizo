from django.db import models
from places.models import Room
from django.contrib.auth import get_user_model
from django.contrib.postgres.fields import ArrayField
import uuid as uuid_lib


class Package(models.Model):
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False)
    is_interval = models.BooleanField(default=False)
    allowable_days = ArrayField(models.SmallIntegerField(), size=7, null=True, blank=True)
    duration_in_hours = models.IntegerField(blank=True, null=True)
    start_hour = models.IntegerField()
    end_hour = models.IntegerField()
    allow_pick_hour = models.BooleanField(default=False)
    room = models.ForeignKey(Room, related_name='packages', on_delete=models.CASCADE)
    price = models.IntegerField(default=0)
    # todo : remove null
    title = models.CharField(max_length=40, null=True)
    description = models.CharField(max_length=400, null=True, blank=True)

    def __str__(self):
        if self.is_interval:
            return self.room.name + ' ' + self.room.place.name + ' selected days '
        return self.room.name + ' ' + self.room.place.name + ' ' + str(self.duration_in_hours)


class Reserve(models.Model):
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False)
    package = models.ForeignKey(Package, on_delete=models.CASCADE)
    reserver = models.ForeignKey(get_user_model(), related_name='reserves', on_delete=models.CASCADE)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    # todo : remove null

    room = models.ForeignKey(Room, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.reserver.username


class Occupation(models.Model):
    reserve = models.ForeignKey(Reserve, on_delete=models.CASCADE)
    start_time = models.DateTimeField(db_index=True)
    end_time = models.DateTimeField(db_index=True)
    # todo : remove null
    room = models.ForeignKey(Room, related_name='occupations', on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.room.name
