from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^package/', ReserveRoom.as_view()),
    url(r'^people_in_room/(?P<uuid>[0-9A-Fa-f-]+)/$', PeopleReserveThisRoom.as_view()),
]