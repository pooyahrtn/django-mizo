from django.contrib import admin
from . import models

@admin.register(models.Package)
class PackageAdmin(admin.ModelAdmin):
    pass

@admin.register(models.Occupation)
class OccupationAdmin(admin.ModelAdmin):
    pass


@admin.register(models.Reserve)
class ReserveAdmin(admin.ModelAdmin):
    pass

