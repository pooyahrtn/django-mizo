# Generated by Django 2.0.2 on 2018-03-19 17:48

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('places', '0010_auto_20180319_1312'),
        ('reserve', '0002_package_allowable_days'),
    ]

    operations = [
        migrations.AddField(
            model_name='occupation',
            name='room',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='places.Room'),
        ),
        migrations.AddField(
            model_name='package',
            name='uuid',
            field=models.UUIDField(db_index=True, default=uuid.uuid4, editable=False),
        ),
        migrations.AddField(
            model_name='reserve',
            name='uuid',
            field=models.UUIDField(db_index=True, default=uuid.uuid4, editable=False),
        ),
    ]
