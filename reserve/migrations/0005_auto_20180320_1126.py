# Generated by Django 2.0.2 on 2018-03-20 11:26

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('reserve', '0004_auto_20180319_1754'),
    ]

    operations = [
        migrations.AlterField(
            model_name='occupation',
            name='end_time',
            field=models.DateTimeField(db_index=True),
        ),
        migrations.AlterField(
            model_name='occupation',
            name='start_time',
            field=models.DateTimeField(db_index=True),
        ),
        migrations.AlterField(
            model_name='package',
            name='room',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='packages', to='places.Room'),
        ),
        migrations.AlterField(
            model_name='reserve',
            name='reserver',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='reserves', to=settings.AUTH_USER_MODEL),
        ),
    ]
