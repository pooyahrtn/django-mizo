from django.db.models import Q
import datetime


def free_space_of_package(package, start_time=None):
    if not start_time:
        start_time = datetime.datetime.now()
    room = package.room
    if package.is_interval:
        return 5
    else:

        end = datetime.datetime.now() + datetime.timedelta(hours=package.duration_in_hours)
        reserves = room.occupations.filter(Q(start_time__gte=start_time) & Q(start_time__lte=end) | Q(end_time__gte=start_time) &
                                           Q(end_time__lte=end) | Q(start_time__lte=start_time) & Q(end_time__gte=end)).count()
        return room.capacity - reserves
