from rest_framework import serializers
from reserve import models
from reserve.utility import free_space_of_package


class PackageSerializer(serializers.ModelSerializer):
    # capacity = serializers.SerializerMethodField()

    class Meta:
        model = models.Package
        fields = ('uuid', 'is_interval', 'allowable_days', 'duration_in_hours', 'start_hour',
                  'end_hour', 'title', 'price', 'description', 'allow_pick_hour', 'duration_in_hours')

    # def get_capacity(self, obj):
    #     return free_space_of_package(obj)


