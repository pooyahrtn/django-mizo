from rest_framework import serializers
from reserve import models
from profiles.serializers import ProfileSerializer


class PeopleReserveThisRoomSerializer(serializers.ModelSerializer):
    person = serializers.SerializerMethodField()

    class Meta:
        model = models.Reserve
        fields = ('person', 'start_time', 'end_time')

    def get_person(self, obj):

        if obj.reserver.public_reservation:
            return ProfileSerializer(obj.reserver, context={'request': self.context['request']}).data


#
# class PeopleReserveThisRoomTimeSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = models.Reserve
#         fields = ('start_time',)
