from rest_framework import serializers
from reserve import models
from places.serializers import ReviewRoomSerializer


class ReviewPackageSerializer(serializers.ModelSerializer):
    room = ReviewRoomSerializer(read_only=True)

    class Meta:
        model = models.Package
        fields = ('uuid', 'allowable_days', 'is_interval', 'duration_in_hours', 'start_hour', 'end_hour',
                  'room', 'title', 'price', 'description', 'allow_pick_hour')


class ReserveSerializer(serializers.ModelSerializer):
    package_uuid = serializers.UUIDField(write_only=True)
    package = ReviewPackageSerializer(read_only=True)
    start_time = serializers.DateTimeField()
    end_time = serializers.DateTimeField(read_only=True)
    uuid = serializers.UUIDField(read_only=True)
    class Meta:
        model = models.Reserve
        fields = ('package', 'start_time', 'end_time', 'package_uuid', 'uuid')


